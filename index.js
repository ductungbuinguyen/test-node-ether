const ethers = require('ethers');
const CONTRACT_ABI = require('./CONTRACT_ABI.json')
const express = require('express')
const app = express();
const port = 3000;
app.get('/', (req, res) => {
    console.log("in controller")
    res.send('api controller express');
});
app.listen(port, () => console.log(`RESTful API server started on: ${port}!`))


const contractAddress = "0x07F1E9BC14636333A851ed066f23d14046FF7D1B";
const provider = new ethers.providers.WebSocketProvider("https://eth-goerli.g.alchemy.com/v2/bHAC_FOvcXipAMEJwnc0LgulflSQ6SUh");

const contract = new ethers.Contract(contractAddress, CONTRACT_ABI, provider)

contract.on('DogMinted', (...args) => {
    console.log('DogMinted event')
    const eventArgs = args[args.length - 1].args
    console.log("tokenId", eventArgs.tokenId.toNumber())
})

contract.on('Transfer', (...args) => {
    console.log('Transfer event')
    const eventArgs = args[args.length - 1].args
    console.log("from", eventArgs.from)
    console.log("to", eventArgs.to)
    console.log("tokenId", eventArgs.tokenId.toNumber())
})
